#include <bmp.h>
#include <image.h>
#include <rot_90ccw.h>
#include <stdio.h>




int main( int argc, char** argv ) {
    if (argc != 3) {
        printf("Wrong number of arguments!\n");
        printf("Usage: %s <source-image> <transformed-image>\n", argv[0]);
    }
    else {
        image_t* orig = image_new_from_file(argv[1], bmp_to_image);
        image_save_to_file("orig_copy.bmp", orig, bmp_from_image);
        image_t* rotated = image_new(orig->height, orig->width);
        rot_ccw90(orig, rotated);
        image_save_to_file(argv[2], rotated, bmp_from_image);

        image_free(orig);
        image_free(rotated);
    }
    return 0;
}
