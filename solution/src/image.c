#include <assert.h>
#include <image.h>
#include <stdlib.h>



image_t* image_new_from_file(char* filename, image_reader_fptr_t img_reader) {
    assert(img_reader != NULL);
    FILE* in = fopen(filename, "rb");
    image_t* img = img_reader(in);
    fclose(in);
    return img;
}

void image_save_to_file(char* filename, image_t* img, image_writer_fptr_t img_writer) {
    assert(img != NULL);
    assert(img_writer != NULL);
    FILE* out = fopen(filename, "wb");
    img_writer(out, img);
    fclose(out);
}

image_t* image_new(size_t width, size_t height) {
    image_t* img = calloc(1, sizeof(image_t));
    img->width = width;
    img->height = height;
    size_t num_pixels = width * height;
    if (num_pixels > 0) {
        img->pix_arr = (pixel_t *) calloc(num_pixels, sizeof(pixel_t));
    }
    return img;
}

void image_free(image_t* img) {
    assert(img != NULL);
    if (img->pix_arr != NULL) {
        free (img->pix_arr);
    }
    free(img);
}

void image_set_pixel(image_t* img, size_t x, size_t y, pixel_t p) {
    uint64_t pix_idx = y * img->width + x;
    img->pix_arr[pix_idx] = p;
}

pixel_t image_get_pixel(image_t* img, size_t x, size_t y) {
    uint64_t pix_idx = y * img->width + x;
    return img->pix_arr[pix_idx];
}


