#include <assert.h>
#include <image.h>

#include <stddef.h>

void rot_ccw90(image_t* src, image_t* dst) {
    assert(src != NULL);
    assert(dst != NULL);
    assert(src->width == dst->height);
    assert(src->height == dst->width);
    for (size_t src_y = 0; src_y < src->width; src_y++) {
        for (size_t src_x = 0; src_x < src->height; src_x++) {
            //size_t src_idx = src_y * src->height + src_x;

            //size_t dst_x = src_y;
            //size_t dst_y = src->height - src_x - 1;
            //size_t dst_idx = dst_y * dst->width + dst_x;
                        //i*new->width+j            (new->width-j-1)*new->height+i
            dst->pix_arr[src_y*dst->width+src_x] = src->pix_arr[(dst->width-src_x-1)*src->width+src_y];
        }
    }
}



