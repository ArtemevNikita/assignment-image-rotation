#include <assert.h>
#include <bmp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>


#define BMP_RGB_BYTES_PER_PIXEL 3
#define BMP_RGB_BITS_PER_PIXEL (BMP_RGB_BYTES_PER_PIXEL * 8)
#define BMP_ROW_ALIGNMENT 4
#define BMP_HEADER_SIZE 54
#define BMP_DIB_HEADER_SIZE 40
#define BMP_FILE_ID 0x4d42
#define BMP_COMPRESSION 0
#define BMP_PIXELS_PER_METER 2834
#define BMP_COLOR_PLANES 1
#define BMP_COLORS_USED 0
#define BMP_COLORS_IMPORTANT 0

typedef struct bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
} __attribute__((packed)) bmp_header_t;

size_t bmp_get_padding_size(size_t width) {
    return (size_t) (BMP_ROW_ALIGNMENT - (width * BMP_RGB_BYTES_PER_PIXEL % BMP_ROW_ALIGNMENT));
}

bmp_header_t bmp_make_header_for_image(image_t* img) {

    size_t padding_size = bmp_get_padding_size(img->width);
    size_t row_byte_size = img->width * BMP_RGB_BYTES_PER_PIXEL + padding_size;
    size_t byte_arr_size = row_byte_size * img->height;

    bmp_header_t h;
    h.bfType = BMP_FILE_ID;
    h.bfileSize = BMP_HEADER_SIZE + byte_arr_size;
    h.bfReserved = 0;
    h.bOffBits = BMP_HEADER_SIZE;
    h.biSize = BMP_DIB_HEADER_SIZE;
    h.biWidth = img->width;
    h.biHeight = img->height;
    h.biPlanes = BMP_COLOR_PLANES;
    h.biBitCount = BMP_RGB_BITS_PER_PIXEL;
    h.biCompression = BMP_COMPRESSION;
    h.biSizeImage = byte_arr_size;
    h.biXPelsPerMeter = BMP_PIXELS_PER_METER;
    h.biYPelsPerMeter = BMP_PIXELS_PER_METER;
    h.biClrUsed = BMP_COLORS_USED;
    h.biClrImportant = BMP_COLORS_IMPORTANT;
    return h;
}

bool bmp_header_is_ok(bmp_header_t* h) {
    if (h->bfType != BMP_FILE_ID) return false;
    if (h->bfileSize < BMP_HEADER_SIZE) return false;
    if (h->bfReserved != 0) return false;
    if (h->bOffBits != BMP_HEADER_SIZE) return false;
    if (h->biSize != BMP_DIB_HEADER_SIZE) return false;
    if (h->biWidth < 1) return false;
    if (h->biHeight < 1) return false;
    if (h->biPlanes != BMP_COLOR_PLANES) return false;
    if (h->biBitCount != BMP_RGB_BITS_PER_PIXEL) return false;
    if (h->biCompression != BMP_COMPRESSION) return false;
    if (h->biSizeImage < 1) return false;
    if (h->biXPelsPerMeter != BMP_PIXELS_PER_METER) return false;
    if (h->biYPelsPerMeter != BMP_PIXELS_PER_METER) return false;
    if (h->biClrUsed != BMP_COLORS_USED) return false;
    if (h->biClrImportant != BMP_COLORS_IMPORTANT) return false;
    return true;
}


image_t* bmp_to_image(FILE* in) {
    assert(in != NULL);

    bmp_header_t header;
    fread(&header, sizeof(bmp_header_t), 1, in);

    assert (bmp_header_is_ok(&header) == true);

    size_t w = header.biWidth;
    size_t h = header.biHeight;
    size_t padding_size = bmp_get_padding_size(w);
    size_t pix_arr_offset = header.bOffBits;

    image_t* img = image_new(w, h);

    fseek(in, pix_arr_offset, SEEK_SET);/*
     for (uint64_t i = 0; i < img->height; i++) {
        fread(&(img->pix_arr[i*img->width]), sizeof(pixel_t), img->width, in);
        fseek(in, (uint8_t)img->width%4, SEEK_CUR);
    }
*/
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            pixel_t p = {0};
            fread(&p, sizeof(pixel_t), 1, in);
            //fscanf_s(in, "%c%c%c", &p.rgb.r, &p.rgb.g, &p.rgb.b);
            image_set_pixel(img, x, y, p);
        }
        if (padding_size != 0) {
            fseek(in, padding_size, SEEK_CUR);
        }
    }

    return img;
}

void bmp_from_image(FILE* out, image_t* img) {
    fseek(out, 0, SEEK_SET);
    bmp_header_t header = bmp_make_header_for_image(img);
    size_t padding_size = bmp_get_padding_size(img->width);
    fwrite(&header, sizeof (bmp_header_t), 1, out);
    for (int y = 0; y < img->height; ++y) {
        for (int x = 0; x < img->width; ++x) {
            pixel_t p = image_get_pixel(img, x, y);
            fwrite(&p, sizeof(pixel_t), 1, out);
            //fprintf(out, "%c%c%c", p.rgb.r, p.rgb.g, p.rgb.b);
        }
        if (padding_size > 0) {
            assert(padding_size < 4);
            uint32_t max_padding = 0xAAAAAAAA;
            fwrite(&max_padding, sizeof(uint8_t), padding_size, out);
        }
    }
}

