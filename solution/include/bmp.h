#pragma once

#include <stdio.h>
#include <image.h>

image_t* bmp_to_image(FILE*);
void bmp_from_image(FILE*, image_t*);
