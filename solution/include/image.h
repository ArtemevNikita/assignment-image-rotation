#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct rgb_t {
    uint8_t b, g, r;
} rgb_t __attribute__((__packed__));

typedef union pixel_t {
    rgb_t rgb;
} pixel_t;

// image_t layout is row-major, with pix_arr[0] being the upper left pixel
typedef struct image_t {
    size_t width;
    size_t height;
    pixel_t* pix_arr;
} image_t;

// function pointers
typedef image_t* (*image_reader_fptr_t) (FILE*);
typedef void (*image_writer_fptr_t) (FILE*, image_t*);

image_t* image_new_from_file(char *filename, image_reader_fptr_t);
void image_save_to_file(char*, image_t*, image_writer_fptr_t);
void image_set_pixel(image_t*, size_t, size_t, pixel_t);
pixel_t image_get_pixel(image_t*, size_t, size_t);
image_t* image_new(size_t, size_t);
void image_free(image_t*);

